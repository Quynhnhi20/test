import {Icon, Thumbnail} from 'native-base';
import React, {Component} from 'react';
import {
  Dimensions,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import Wrapper from '../wrapper';
import styles from './styles';
import images from '../../constants/images';
import {colors} from '../../constants/theme';
import SelectPopup from '../popups/SelectPopup';
import ItemSidebar from '../itemSidebar';
const {width, height} = Dimensions.get('window');

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      popup: false,
    };
  }
  render() {
    const {navigation} = this.props;
    return (
      <Wrapper flex={1}>
        <Wrapper style={styles.header} row center>
          <Wrapper style={styles.circle}>
            <Image
              source={images.LOGO_BE}
              style={styles.logoBe}
              resizeMode="contain"
            />
          </Wrapper>
          <Wrapper style={styles.main}>
            <Text style={styles.name}>Bạch Nhật</Text>
            <Text style={styles.sdt}>+84965043781</Text>
          </Wrapper>
        </Wrapper>
        <ScrollView style={{width: '100%'}}>
          <Wrapper style={styles.content}>
            <ItemSidebar
              title="Trang chủ"
              icon="home"
              typeIcon="Ionicons"
              backgroundColorItem
              colorIcon
              navigation={navigation}
            />
            <ItemSidebar
              title="Đổi hình nền"
              icon="image"
              typeIcon="FontAwesome"
              navigation={navigation}
            />
            <ItemSidebar
              title="Đổi tiêu đề"
              icon="format-title"
              typeIcon="MaterialCommunityIcons"
              navigation={navigation}
            />
            <ItemSidebar
              title="Đổi ngày bắt đầu"
              icon="md-today"
              typeIcon="Ionicons"
              navigation={navigation}
            />
            <ItemSidebar
              title="Đổi thông tin hiển thị 1"
              icon="contact"
              typeIcon="Ionicons"
              navigation={navigation}
            />
            <ItemSidebar
              title="Đổi thông tin hiển thị 2"
              icon="contact"
              typeIcon="Ionicons"
              navigation={navigation}
            />
            <ItemSidebar
              title="Đổi font chữ"
              icon="font"
              typeIcon="FontAwesome"
              navigation={navigation}
            />
            <ItemSidebar
              title="Đổi status tình yêu"
              icon="statusnet"
              typeIcon="Zocial"
              navigation={navigation}
            />
            <ItemSidebar
              title="Đánh giá 5 sao"
              icon="star"
              typeIcon="FontAwesome"
              navigation={navigation}
            />
            <ItemSidebar
              title="Fanpage Facebook"
              icon="facebook"
              typeIcon="FontAwesome"
              navigation={navigation}
            />
            <ItemSidebar
              title="Tạm ẩn quảng cáo"
              icon="md-eye-off"
              typeIcon="Ionicons"
              navigation={navigation}
            />
            <ItemSidebar
              title="Chia sẻ bạn bè"
              icon="sharealt"
              typeIcon="AntDesign"
              navigation={navigation}
            />
            <ItemSidebar
              title="Phản hồi và hỗ trợ"
              icon="help-buoy"
              typeIcon="Ionicons"
              navigation={navigation}
            />
            <ItemSidebar
              title="Xem thêm"
              icon="more"
              typeIcon="Ionicons"
              navigation={navigation}
            />
          </Wrapper>
        </ScrollView>
        <Wrapper right flex={1} style={styles.made}>
          <Text style={styles.textmade}>Phiên bản 1.0</Text>
        </Wrapper>
      </Wrapper>
    );
  }
}

export default SideBar;
