import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../constants/theme";
const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  header:{
    width:'100%',
    height: height*0.22,
    backgroundColor: colors.TEXTBE,
    paddingHorizontal:20,
    paddingTop:20
  },
  circle:{
    width: 90,
    height:90,
    backgroundColor:colors.WHITE,
    borderRadius:60,
  },
  main:{
    marginLeft:10
  },
  name:{
    color:colors.WHITE,
    fontWeight:'600',
    fontSize:18,
    paddingVertical:5
  },
  sdt:{
    color: colors.WHITE,
    fontWeight: '500',
    fontSize: 14,
  },
  logoBe:{
    width:'100%',
    height:"100%"
  },
  touchItem:{
    width:'100%',
    height:50,
    paddingLeft:20,
  },
  titleItem:{
    marginLeft:20,
    fontSize:16,
    fontWeight: '500'
  },
  content:{
  },
  padding:{
    paddingTop:8
  },
  made:{
    padding:20,
  },
  textmade:{
    fontSize: 12,
    color: colors.BORDER,
    fontWeight:'500'
  },
  iconItem:{
    color: colors.BORDER,
    fontSize:25,
    width:30
  }
});

export default styles;
