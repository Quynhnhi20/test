import React from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import { colors } from '../../constants/theme';
import Wrapper from '../wrapper';
import CenterPopup from './CenterPopup';
import { Icon } from 'native-base';

const { width } = Dimensions.get('window')

const SelectPopup = ({
  title = {},
  visible,
  close = {},
  card = {},
  buttons = {}
}) => {
  return (
    <CenterPopup visible={visible}>
      <Wrapper style={styles.wrapPop}>
        <Wrapper style={{ justifyContent: 'space-between' }} row>
          <Wrapper style={styles.title}>
            <Text style={styles.textTitle}>{title}</Text>
          </Wrapper>
          <TouchableOpacity onPress={close.onPress}>
            <Icon name="close" style={{ color: colors.PRIMARY_DARK }} />
          </TouchableOpacity>
        </Wrapper>
        <Wrapper style={styles.card}>{card}</Wrapper>
      </Wrapper>
    </CenterPopup>
  );
};


const styles = StyleSheet.create({
  wrapPop: {
    width: width * 0.9,
    backgroundColor: colors.WHITE,
    borderRadius: 10,
    padding: 20,
    paddingBottom: 0,
  },
  title: {},
  textTitle: {
    fontSize: 20,
    color:colors.PRIMARY_DARK,
    fontWeight:'500'
  },
  body: {
    color: 'blue',
    fontSize: 16,
  },

  card: {
    marginVertical: 5
  }
});

export default SelectPopup;