import React from "react";
import { Modal, View, Dimensions } from "react-native";
const { width, height } = Dimensions.get('window')

const CenterPopup = ({ children, visible }) => {
  return (
    <Modal animationType="fade" transparent visible={visible}>
      <View
        style={[{
          justifyContent: "center",
          alignItems: "center",
          height: height,
          width: width,
          backgroundColor: "rgba(0, 0, 0, 0.6)"
        }]}
      >
        {children}
      </View>
    </Modal>
  );
};

export default CenterPopup;
