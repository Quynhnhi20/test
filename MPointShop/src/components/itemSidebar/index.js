import React, {useState, Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, TextInput} from 'react-native';
import Wrapper from '../../components/wrapper';
import {colors} from '../../constants/theme';
import {Icon} from 'native-base';
import SelectPopup from '../popups/SelectPopup';
// import {CheckBox} from 'react-native-elements';

const ItemSidebar = ({
  title,
  icon,
  typeIcon,
  backgroundColorItem,
  colorIcon,
  navigation,
}) => {
  const [popup, setPopup] = useState(false);
  const [ten, onChangeTen] = React.useState('');

  const onTouch = () => {
    setPopup(true);
    navigation.closeDrawer();
  };

  return (
    <>
      <TouchableOpacity style={styles.padding} onPress={() => onTouch()}>
        <Wrapper
          row
          center
          style={[
            styles.touchItem,
            backgroundColorItem ? {backgroundColor: colors.GRAY2} : null,
          ]}>
          <Icon
            name={icon}
            type={typeIcon}
            style={[styles.iconItem, colorIcon ? {color: 'black'} : null]}
          />
          <Text style={styles.titleItem}>{title}</Text>
        </Wrapper>
      </TouchableOpacity>
      <SelectPopup
        visible={popup}
        title={title}
        close={{onPress: () => setPopup(!popup)}}
        card={
          <>
            <Text>{ten}</Text>
            <TextInput
              style={styles.inputName}
              onChangeText={text => onChangeTen(text)}
              value={ten}
              placeholder="Nhập tên"
              placeholderTextColor="gray"
            />
            {/* <Wrapper row>
              <CheckBox title="Nam" containerStyle={styles.checker} />
              <CheckBox title="Nữ" containerStyle={styles.checker} />
            </Wrapper> */}
          </>
        }
        buttons={[
          {
            title: 'OK',
            color: colors.PRIMARY,
            onPress: () => setPopup(!popup),
          },
        ]}
      />
    </>
  );
};

export default ItemSidebar;

const styles = StyleSheet.create({
  padding: {
    paddingTop: 8,
  },
  iconItem: {
    color: colors.BORDER,
    fontSize: 25,
    width: 30,
  },
  touchItem: {
    width: '100%',
    height: 50,
    paddingLeft: 20,
  },
  titleItem: {
    marginLeft: 20,
    fontSize: 16,
    fontWeight: '500',
  },
  inputName: {
    width: '100%',
    height: 35,
    borderColor: colors.BLACK,
    borderWidth: 0.5,
    borderRadius: 5,
    paddingLeft: 5,
  },
  checker:{
    borderRadius:0,
    backgroundColor:null,
    borderWidth:0,
    marginLeft: 0,
    padding:0,
    marginVertical: 5,
  }
});
