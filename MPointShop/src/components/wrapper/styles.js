import { StyleSheet } from "react-native";
import { colors } from "../../constants/theme";

const styles = StyleSheet.create({
  block: {
    display: "flex"
  },
  row: {
    flexDirection: "row"
  },
  center: {
    alignItems: "center"
  },
  bottom: {
    alignItems: "flex-end"
  },
  top: {
    alignItems: "flex-start"
  },
  stretch: {
    alignItems: "stretch"
  },
  middle: {
    justifyContent: "center"
  },
  right: {
    justifyContent: "flex-end"
  },
  rightJC: {
    justifyContent: "flex-start"
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 2,
      height: 4
    },
    shadowOpacity: 0.2,
    shadowRadius: 7,

    elevation: 5
  }
});

export default styles;
