import React, { Component } from "react";
import { TouchableOpacity, View } from "react-native";
import styles from "./styles";

export default class Wrapper extends Component {
  render() {
    const {
      flex,
      row,
      center,
      middle,
      right,
      space,
      style,
      bottom,
      top,
      stretch,
      children,
      onPress,
      shadow,
      ...props
    } = this.props;
    let Container = View;
    if (onPress) Container = TouchableOpacity;
    const blockStyles = [
      styles.block,
      flex && { flex },
      flex === "disabled" && { flex: 0 },
      center && styles.center,
      middle && styles.middle,
      right && styles.right,
      bottom && styles.bottom,
      top && styles.top,
      stretch && styles.stretch,
      space && { justifyContent: `space-${space}` },
      row && styles.row,
      shadow && styles.shadow,
      style
    ];

    return (
      <Container
        style={blockStyles}
        {...props}
        onPress={() => {
          if (onPress) onPress();
        }}
      >
        {children}
      </Container>
    );
  }
}
