import React, { Component } from "react";
import { View, TouchableOpacity, BackHandler, Text } from "react-native";
import helpers from "../../../globals/helpers";
import { connect } from "react-redux";
import styles from './styles'
class Messagebox extends Component {
  componentWillReceiveProps(nextState) {
    if (!nextState.showMessage) {
      BackHandler.removeEventListener(
        "hardwareBackPress",
        this.handleBackPress
      );
    } else {
      BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
    }
  }

  handleBackPress = () => {
    helpers.hideMessageBox();
    return true;
  };

  render() {
    let { titleMes, contentMess } = this.props;
    if (this.props.showMessage) {
      return (
        <View
          style={styles.container}
        >
          <View
            style={styles.wrapMessage}
          >
            <View
              style={styles.wrapTitle}
            >
              <Text style={styles.titleMessage}>
                {titleMes ? titleMes : "Thông báo"}
              </Text>
            </View>
            <View style={{ marginTop: 8 }}>
              <Text
                style={styles.contentMessage}
              >
                {contentMess}
              </Text>
            </View>
            <View style={styles.wrapButton}>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => {
                  helpers.hideMessageBox();
                }}
                style={styles.buttonMessage}
              >
                <Text style={styles.txtButton}>ĐÓNG</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      );
    } else {
      return null;
    }
  }
}

mapStateToProps = state => {
  return {
    showMessage: state.uiState.showMessage,
    contentMess: state.uiState.contentMess,
    titleMes: state.uiState.titleMes
  };
};

export default connect(mapStateToProps)(Messagebox);
