import { StyleSheet } from 'react-native'
import Layout from '../../../constants/layout';
const styles = StyleSheet.create({
    container: {
        width: Layout.window.width,
        height: Layout.window.height,
        backgroundColor: "transparent",
        zIndex: 10,
        backgroundColor: "rgba(0,0,0,0.5)",
        justifyContent: "center",
        alignItems: "center"
    },
    wrapLoading: {
        backgroundColor: '#fff',
        padding: 30,
        borderRadius: 10,
        // width: Layout.window.width / 3,
        // height: Layout.window.width / 3
    },
    titleLoading: {
        marginTop: 10
    }
})

export default styles;