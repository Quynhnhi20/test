import React, { Component } from "react";
import { View, TouchableOpacity, BackHandler, Text } from "react-native";
import helpers from "../../../globals/helpers";
import { connect } from "react-redux";
import styles from './styles'
class ConfirmBox extends Component {
  componentWillReceiveProps(nextState) {
    if (!nextState.showConfirmBox) {
      BackHandler.removeEventListener(
        "hardwareBackPress",
        this.handleBackPress
      );
    } else {
      BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
    }
  }

  handleBackPress = () => {
    helpers.hideConfirm();
    return true;
  };
  render() {
    let { titlebntHuy, titlebntOK, contentConf, titleConf } = this.props;
    if (this.props.showConfirmBox) {
      return (
        <View
          style={styles.container}
        >
          <View
            style={styles.wrapConfirm}
          >
            <View
              style={styles.wrapTitle}
            >
              <Text style={styles.titleConfirm}>
                {titleConf}
              </Text>
            </View>
            <View style={styles.wrapContentConfirm}>
              <Text
                style={styles.contentConfirm}
              >
                {contentConf}
              </Text>
            </View>
            <View
              style={styles.wrapButton}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => {
                  helpers.hideConfirm();
                  this.props.btnHuy();
                }}
                style={[styles.buttonConfirm]}
              >
                <Text style={styles.txtButton}>{titlebntHuy ? titlebntHuy.toUpperCase() : "TỪ CHỐI"}</Text>
              </TouchableOpacity>
              {/* <Text style={{ width: 10 }} /> */}
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => {
                  helpers.hideConfirm();
                  this.props.btnOK();
                }}
                style={[styles.buttonConfirm,]}
              >
                <Text style={styles.txtButton}>
                  {titlebntOK ? titlebntOK.toUpperCase() : "ĐỒNG Ý"}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      );
    } else {
      return null;
    }
  }
}

mapStateToProps = state => {
  return {
    showConfirmBox: state.uiState.showConfirmBox,
    contentConf: state.uiState.contentConf,
    titleConf: state.uiState.titleConf,
    btnHuy: state.uiState.btnHuy,
    btnOK: state.uiState.btnOK,
    titlebntHuy: state.uiState.titlebntHuy,
    titlebntOK: state.uiState.titlebntOK
  };
};

export default connect(mapStateToProps)(ConfirmBox);
