import { StyleSheet } from 'react-native'
import Layout from '../../../constants/layout';
import { colors } from '../../../constants/theme'
const styles = StyleSheet.create({
    container: {
        width: Layout.window.width,
        height: Layout.window.height,
        backgroundColor: "rgba(0, 0, 0, 0.6)",
        justifyContent: "center",
        alignItems: "center",
        zIndex: 10

    },
    wrapConfirm: {
        minWidth: "70%",
        maxWidth: '80%',
        backgroundColor: "white",
        borderRadius: 5,
        padding: 15,
        paddingBottom: 10,
        justifyContent: 'space-between'
    },
    wrapTitle: {
        flexDirection: "row",
        paddingBottom: 3
    },
    titleConfirm: {
        fontWeight: "bold", fontSize: 20,
    },
    wrapContentConfirm: { marginTop: 8 },
    contentConfirm: { fontSize: 16, lineHeight: 20, color: '#555555' },
    wrapButton: {
        flexDirection: "row",
        marginTop: 20,
        justifyContent: 'flex-end',

    },
    buttonConfirm: {
        height: 35,
        padding: 8,
        paddingRight: 0,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        minWidth: 80,

    },
    txtButton: { color: colors.PRIMARY, fontSize: 14, fontWeight: 'bold' }
})

export default styles;