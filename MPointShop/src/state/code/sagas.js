import { call, put, takeLatest, all } from "redux-saga/effects";
import {
    login,
    history,
    setAuthToken,
    refreshToken,
    getCodeInfo
} from "../../utils/api";
import * as Types from "./types";
import helpers from "../../globals/helpers";

function* getInfoCode(action) {
    try {
        const codeInfo = yield call(getCodeInfo, action.payload);
        yield put({
            type: Types.GET_COCE_INFO_SUCCESS,
            payload: codeInfo
        });
    } catch (error) {
        console.log(error.response)
        // helpers.showMessage(error)

    }
}

export function* watchgetInfoCode() {
    yield takeLatest(Types.GET_COCE_INFO, getInfoCode);
}