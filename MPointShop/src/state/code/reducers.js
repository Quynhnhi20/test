import * as Types from "./types";

const initialState = {
    infoShop: {}
};

const codeReducer = (state = initialState, action) => {
    switch (action.type) {
        case Types.GET_COCE_INFO_SUCCESS:
            return {
                ...state,
                infoShop: action.payload
            };
        default:
            return state;
    }
};

export default codeReducer;
