import * as Types from "./types";

const initialState = {
  historyShop:'',
  profile: '',
  isAuthenticated: false,
  mess: "",
  isFetching: false,
  isCheckout: false,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.LOGIN_SUCCESS:
      return {
        ...state,
        profile: action.payload
      };
    case Types.HISTORY_SHOP_SUCCESS:
      console.log(action.payload)
      return {
        ...state,
        historyShop: action.payload
      };
    // case Types.LOGOUT_SUCCESS:
    //   return { ...initialState };
    // case Types.LOGIN_ERROR:
    //   return {
    //     ...state,
    //     mess: "Wrong code!!!"
    //   };
    // case Types.GET_SESSION_SUCCESS:
    //   return {
    //     ...state,
    //     session: action.payload
    //   };
    // case Types.FETCHING:
    //   return {
    //     ...state,
    //     isFetching: true
    //   };
    // case Types.FETCHING_SUCCESS:
    //   return {
    //     ...state,
    //     isFetching: false
    //   };
    // case Types.TADA:
    //   return {
    //     ...state,
    //     isTada: true
    //   };
    // case Types.TADA_SUCCESS:
    //   return {
    //     ...state,
    //     isTada: false
    //   };
    // case Types.GET_CURRENT:
    //   return {
    //     ...state,
    //     isFetching: false
    //   };
    // case Types.CHECKOUT_SUCCESS:
    //   return {
    //     ...state,
    //     isCheckout: true,
    //     distance: action.payload.distance,
    //     img_checking: action.payload.image_check_out
    //   }
    // case Types.CHECKIN_SUCCESS:
    //   return {
    //     ...state,
    //     isCheckout: false,
    //     statusCheckin: { checking_id: action.payload.checking_id},
    //     distance:action.payload.distance,
    //     img_checking: action.payload.image_check_in
    //   }
    // case Types.GET_WORK_PLACE_SUCCESS:
    //   return {
    //     ...state,
    //     workPlace: action.payload,
    //   };
    // case Types.GET_CURRENT_WORK_PLACE_SUCCESS:
    //   const wps = action.payload.data?Object.values(action.payload.data):[]
    //   return {
    //     ...state,
    //     currentWorkPlace: wps,
    //     shift_works: wps.map((el, idx) => el.shift_work)
    //   };
    // case Types.GET_STATUS_CHECKIN:
    //   return {
    //     ...state,
    //     statusCheckin: action.payload,
    //     isCheckout: action.payload == '' ? true : false
    //   };
    // case Types.UP_IMAGE_SUCCESS:
    //   return {
    //     ...state,
    //     dataIMG: action.payload,
    //   };
    // case Types.GET_WORK_CALENDAR_SUCCESS:
    //   return {
    //     ...state,
    //     workCalendar: action.payload,
    //   };

    
    default:
      return state;
  }
};

export default userReducer;
