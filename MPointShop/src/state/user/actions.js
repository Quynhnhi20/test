import * as Types from "./types";

export const loginUser = data => {
  return {
    type: Types.LOGIN,
    payload: data
  };
};

export const refreshToken = (payload) => {
  return {
    type: Types.REFRESH_TOKEN,
    payload
  };
};

export const historyShop = data => {
  return {
    type: Types.HISTORY_SHOP,
    payload: data
  };
};

// export const loginUser = data => {
//   return {
//     type: Types.LOGIN,
//     payload: data
//   };
// };


// export const logoutUser = () => {
//   return {
//     type: Types.LOGOUT
//   };
// };

// export const getSession = () => {
//   return {
//     type: Types.GET_SESSION
//   };
// };

// export const checkIn = data => {
//   return {
//     type: Types.CHECKIN,
//     payload: data
//   };
// };

// export const fetchRequest = () => {
//   return {
//     type: Types.FETCHING,
//   };
// };

// export const fetchSuccess = () => {
//   return {
//     type: Types.FETCHING_SUCCESS,
//   };
// };

// export const tadaRequest = () => {
//   return {
//     type: Types.TADA,
//   };
// };

// export const tadaSuccess = () => {
//   return {
//     type: Types.TADA_SUCCESS,
//   };
// };

// export const checkOut = data => {
//   return {
//     type: Types.CHECKOUT,
//     payload: data
//   };
// };

// export const getCompanyInfo = company_code => {
//   return {
//     type: Types.GET_COMPANY,
//     payload: company_code
//   };
// };

// export const uploadImg = (dataImg) => {
//   return {
//     type: Types.UP_IMAGE,
//     payload: dataImg
//   };
// };

// export const signUp = data => {
//   return {
//     type: Types.SIGN_UP,
//     payload: data
//   };
// };


// export const getWorkCalendar = data => {
//   return {
//     type: Types.GET_WORK_CALENDAR,
//     payload: data
//   };
// };
