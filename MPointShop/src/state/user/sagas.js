import { call, put, takeLatest, all } from "redux-saga/effects";
import {
  login,
  history,
  setAuthToken,
  refreshToken
} from "../../utils/api";
import * as Types from "./types";
import storeT from "../../utils/store";
import { sleep } from "../../utils/utilities";
import { Alert } from "react-native";
import helpers from "../../globals/helpers";
// import { showMessage, hideMessage } from "react-native-flash-message";
function* userLoginSaga(action) {
  // console.log(action)
  try {
    const loginUser = yield call(login, action.payload);
    const access_token = loginUser.token.token
    yield put({
      type: Types.LOGIN_SUCCESS,
      payload: loginUser
    });
    storeT.setToken(access_token);
    console.log(access_token)
    setAuthToken(access_token);
    action.payload.success.navigate('TabScreens')
  } catch (error) {
    Alert.alert("Thông báo", "Nhập sai tài khoản !");
    console.log(error)
  }
}

export function* watchUserLogin() {
  yield takeLatest(Types.LOGIN, userLoginSaga);
}


function* historys(action) {
  console.log(action)
  try {
    const hstr = yield call(history, action.payload);
    console.log(hstr)
    if (hstr.code == 401) {
      Alert.alert("Thông báo", hstr.msg);
    }
    yield put({
      type: Types.HISTORY_SHOP_SUCCESS,
      payload: hstr
    });
  } catch (error) {
    Alert.alert("Thông báo", "Lỗi hệ thống!");
    console.log(error)
  }
}

export function* watchHistory() {
  yield takeLatest(Types.HISTORY_SHOP, historys);
}


function* refreshTokenUser(action) {
  console.log(action.payload.token, 'token')
  try {
    helpers.showLoading()
    yield setAuthToken(action.payload.token)
    const loginUser = yield call(refreshToken);
    helpers.hideLoading()
    yield put({
      type: Types.LOGIN_SUCCESS,
      payload: loginUser
    });

    action.payload.success.navigate('TabScreens')
  } catch (error) {
    Alert.alert("Thông báo", "Nhập sai tài khoản !");
    console.log(error)
  }
}

export function* watchrefreshTokenUser() {
  yield takeLatest(Types.REFRESH_TOKEN, refreshTokenUser);
}



































function* getCompanySaga(action) {
  try {
    yield put({
      type: Types.FETCHING
    });
    const data = yield call(getCompany, action.payload);
    yield put({
      type: Types.GET_COMPANY_SUCCESS,
      payload: data
    });
    storeT.setCompanyID(`${data.id}`);
    // Actions.tada({ company_id: data.id + "" });
  } catch (error) {
    Alert.alert("Thông báo", "Mã công ty không chính xác !");
    yield put({
      type: Types.GET_COMPANY_ERROR
    });
  } finally {

    yield put({
      type: Types.FETCHING_SUCCESS
    });
  }
}

export function* watchGetCompany() {
  yield takeLatest(Types.GET_COMPANY, getCompanySaga);
}

function* userLogoutSaga() {
  try {
    yield call(setAuthToken);
    yield put({
      type: Types.LOGOUT_SUCCESS
    });
    // yield call(Actions.login);
  } catch (error) {
    console.log("logout");
    console.log(error);
    yield put({
      type: Types.LOGOUT_ERROR
    });
  }
}

export function* watchUserLogout() {
  yield takeLatest(Types.LOGOUT, userLogoutSaga);
}

function* getSessionSaga() {
  try {
    const data = yield call(getSession);
    yield put({
      type: Types.GET_SESSION_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: Types.GET_SESSION_ERROR
    });
  }
}

export function* watchGetSession() {
  yield takeLatest(Types.GET_SESSION, getSessionSaga);
}

function* checkInSaga(actions) {
  try {
    yield put({
      type: Types.FETCHING
    });
    const { distance, compressedImg, ...rest } = actions.payload;
    const image_check_in = yield call(uploadImg, compressedImg)
    const checking_id = yield call(checkin, { ...rest, image_check_in })
    yield put({
      type: Types.CHECKIN_SUCCESS,
      payload: {
        checking_id,
        image_check_in,
        distance
      }
    });
    // Actions.checked()
  } catch (error) {
    console.log(error.response);
  } finally {
    yield put({
      type: Types.FETCHING_SUCCESS
    });
  }
}

export function* watchCheckIn() {
  yield takeLatest(Types.CHECKIN, checkInSaga);
}

function* checkOutSaga(actions) {
  try {
    yield put({
      type: Types.FETCHING
    });
    const { distance, compressedImg, ...rest } = actions.payload;
    const image_check_out = yield call(uploadImg, compressedImg)
    yield call(checkout, { ...rest, image_check_out })
    yield put({
      type: Types.CHECKOUT_SUCCESS,
      payload: {
        image_check_out,
        distance
      }
    });
    // Actions.checked()
  } catch (error) {
    console.log(error.response);
  } finally {
    yield put({
      type: Types.FETCHING_SUCCESS
    });
  }
}

export function* watchCheckOut() {
  yield takeLatest(Types.CHECKOUT, checkOutSaga);
}

function* signUpSaga(actions) {
  const { data, avatar } = actions.payload
  try {
    const image = yield call(uploadImg, avatar)
    const datas = { ...data, image }
    const signUp = yield call(callSignUp, datas)
    yield put({
      type: Types.SIGN_UP_SUCCESS,
      payload: signUp
    });
  } catch (error) {
    console.log(error)
    Alert.alert("Thông báo", "Mã công ty không chính xác !");
  }
}

export function* watchSignUp() {
  yield takeLatest(Types.SIGN_UP, signUpSaga);
}


function* getWorkCalendarWorker(action) {

  try {
    yield put({
      type: Types.FETCHING
    });
    const calendar = yield call(getWorkCalendar, action.payload)
    // console.log(calendar)
    yield put({
      type: Types.GET_WORK_CALENDAR_SUCCESS,
      payload: calendar
    });
  } catch (error) {
    console.log(error.response);
  } finally {
    yield put({
      type: Types.FETCHING_SUCCESS
    });
  }
}

export function* watGetWorkCalendar() {
  yield takeLatest(Types.GET_WORK_CALENDAR, getWorkCalendarWorker);
}