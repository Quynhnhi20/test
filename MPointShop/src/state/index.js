import { combineReducers } from "redux";
import { all, fork } from "redux-saga/effects";
import { userReducers, userSagas } from "./user";
import uiReducer from '../state/ui/reducer';
import { codeReducers, codeSagas } from './code'
const rootReducer = combineReducers({
  user: userReducers,
  uiState: uiReducer,
  codeState: codeReducers
});
export default rootReducer;

export function* rootSaga() {
  yield all(
    [
      ...Object.values(codeSagas),
      ...Object.values(userSagas)
    ].map(fork)
  );
}
