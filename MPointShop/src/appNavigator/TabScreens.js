import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import History from '../screenes/History';
import Extend from '../screenes/Extend';
import Identification from '../screenes/Identification';
import Profile from '../screenes/Profile';
// import Icon from 'react-native-vector-icons/Ionicons';
import {Icon} from 'native-base'
import React, { Component } from 'react'
import { colors } from '../constants/theme';


const TabScreens = createBottomTabNavigator(
  {
    Identification: {
      screen: Identification,
      navigationOptions: {
        tabBarLabel: 'Xác thực',
      },
    },
    Extend: {
      screen: Extend,
      navigationOptions: {
        tabBarLabel: 'Thêm',
      },
    },
    History: {
      screen: History,
      navigationOptions: {
        tabBarLabel: 'Lịch sử',
      },
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        tabBarLabel: 'Tài khoản',
      },
    },
  },
  {
    initialRouteName: 'Identification',
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        let iconName;
        if (routeName === 'Identification') {
          iconName = `checkmark-circle-outline`;
        } else if (routeName === 'History') {
          iconName = `wallet`;
        } else if (routeName === 'Profile') {
          iconName = `contact`;
        } else if (routeName === 'Extend') {
          iconName = `add-circle`;
        }
        return <Icon name={iconName} size={25} style={{color: tintColor}} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: colors.PRIMARY_DARK,
      inactiveTintColor: 'gray',
      tabStyle: {
      },
      style: {
        borderTopWidth: 0,
      },
      labelStyle: {
      },
    },
  },
);

export default TabScreens 

