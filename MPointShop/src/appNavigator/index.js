import { createStackNavigator } from 'react-navigation-stack';
import StackScreens from './StackScreens';
import DrawerScreens from './DrawerScreens';
import TabScreens from './TabScreens';
import Endow from '../screenes/Endow';
import Invoice from '../screenes/Invoice';
import Phone from '../screenes/Phone';
import QRScan from '../screenes/QRScan';
import EditUserLink from '../screenes/EditUserLink';


const AppNavigator = createStackNavigator(
  {
    StackScreens: {
      screen: StackScreens,
      navigationOptions: {
        header: null,
      },
    },
    TabScreens: {
      screen: TabScreens,
      navigationOptions: {
        header: null,
      },
    },
    QRScan: {
      screen: QRScan,
      navigationOptions: {
        header: null,
      },
    },
    Endow,
    Invoice,
    Phone,
    EditUserLink
  },
  {
    initialRouteName: 'StackScreens',
    // initialRouteName: 'TabScreens',
  },
);

export default AppNavigator