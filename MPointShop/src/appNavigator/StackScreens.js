import { createStackNavigator } from 'react-navigation-stack';

import Start from '../screenes/Start';
import Login from '../screenes/Login';


const StackScreens = createStackNavigator(
  {
    Start,
    Login,
  },
  {
    initialRouteName: 'Login',
  },
);

export default StackScreens