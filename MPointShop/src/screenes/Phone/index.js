import React, {Component, useState} from 'react';
import {
  Text,
  View,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import images from '../../constants/images';
import Wrapper from '../../components/wrapper';
import styles from './styles';
import {colors} from '../../constants/theme';
import {Icon} from 'native-base';
const {width, height} = Dimensions.get('window');

const Phone = () => {
  const [CodeInvoice, setCodeInvoice] = React.useState('');
  const [Invoice, setInvoice] = React.useState('');
  const [Phone, setPhone] = React.useState('');

  return (
    <ImageBackground source={images.BGLOGIN} style={{width, height}}>
      <Wrapper flex={1} style={{padding: 10}}>
        <Wrapper style={styles.content}>
          <Wrapper row center style={styles.item}>
            <Icon name="call" style={styles.icon}></Icon>
            <TextInput
              style={styles.textInvoice}
              onChangeText={textPhone => setInvoice(textPhone)}
              value={Phone}
              placeholder="Nhập số điện thoại"
              placeholderTextColor={colors.GRAY}
            />
          </Wrapper>
          <Wrapper row center style={styles.item}>
            <Icon name="wallet" style={styles.icon}></Icon>
            <TextInput
              style={styles.textInvoice}
              onChangeText={textInvoice => setInvoice(textInvoice)}
              value={Invoice}
              placeholder="Tổng hóa đơn (VND)(nếu có)"
              placeholderTextColor={colors.GRAY}
            />
          </Wrapper>
          <Wrapper row center style={[styles.item, {marginBottom: 0}]}>
            <Icon name="barcode" style={styles.icon}></Icon>
            <TextInput
              style={styles.textEndow}
              onChangeText={TInvoice => setCodeInvoice(TInvoice)}
              value={CodeInvoice}
              placeholder="Mã hóa đơn (nếu có )"
              placeholderTextColor={colors.GRAY}
            />
            <Wrapper flex={1} center middle>
              <Text style={styles.or}>Hoặc</Text>
            </Wrapper>
            <TouchableOpacity>
              <Wrapper center middle style={styles.touchScan} flex={1}>
                <Text style={styles.textScan}>Quét QR</Text>
              </Wrapper>
            </TouchableOpacity>
          </Wrapper>
        </Wrapper>
        <TouchableOpacity>
          <Wrapper style={styles.touchSubmit} center middle>
            <Text style={styles.textSubmit}>Xác thực</Text>
          </Wrapper>
        </TouchableOpacity>
      </Wrapper>
    </ImageBackground>
  );
};

export default Phone;

Phone.navigationOptions = ({navigation}) => ({
  title: 'Xác thực bằng số điện thoại',
  headerStyle: {
    backgroundColor: colors.PRIMARY_DARK,
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
});
