import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../constants/theme';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  content: {
    width: '100%',
    backgroundColor: colors.WHITE,
    borderRadius: 8,
    padding: 10,
  },
  textEndow: {
    width: '54%',
    height: 35,
    borderRadius: 8,
    borderColor: colors.BLACK,
    borderWidth: 0.3,
    paddingLeft: 40,
  },
  textInvoice: {
    width: '100%',
    height: 35,
    borderRadius: 8,
    borderColor: colors.BLACK,
    borderWidth: 0.3,
    paddingLeft: 40,
  },
  item: {
    marginBottom: 10,
  },
  icon: {
    fontSize: 25,
    position: 'absolute',
    left: 10,
    color: 'gray',
  },
  or: {
    fontWeight: '600',
    fontSize: 16,
  },
  touchScan: {
    backgroundColor: colors.PRIMARY_DARK,
    borderRadius: 8,
    height: 35,
  },
  textScan: {
    color: colors.WHITE,
    paddingHorizontal: 20,
  },
  line: {
    borderBottomColor: colors.BLACK,
    borderBottomWidth: 1,
    marginBottom: 10,
  },
  imgDefaul: {
    width: '50%',
    height: 170,
    borderRadius: 8,
  },
  touchPic: {
    backgroundColor: colors.PRIMARY_DARK,
    height: 35,
    width: '100%',
    marginLeft: 10,
    borderRadius: 8,
  },
  touchSubmit: {
    width: '100%',
    height: 40,
    borderRadius: 8,
    backgroundColor: colors.PRIMARY_DARK,
    marginTop: 20,
  },
  textSubmit: {
    color: colors.WHITE,
    fontWeight: '600',
  },
});

export default styles;
