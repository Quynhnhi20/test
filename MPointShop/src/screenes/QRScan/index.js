import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import Layout from '../../constants/layout';
import { colors } from '../../constants/theme';
import { RNCamera } from 'react-native-camera';

let scan = true
let timmeOut = null
export default class ScanScreen extends Component {


  onBarCodeRead = (data) => {
    if (scan) {
      scan = false
      this.props.navigation.state.params.checkOut(data.data)
      this.props.navigation.pop()
      timmeOut = setTimeout(() => {
        scan = true
      }, 5000)
    }


  }

  componentWillUnmount() {
    clearTimeout(timmeOut)
    scan = true
  }

  render() {
    return (
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={{ flex: 1 }}
          captureAudio={false}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.off}
          onBarCodeRead={this.onBarCodeRead}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}>
          <View
            style={{
              width: '100%',
              backgroundColor: 'rgba(0,0,0,0.5)',
              height: Layout.window.height / 3,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{ color: '#fff', fontSize: 15 }}>
              Di chuyển camera tới vùng chứa mã để quét{' '}
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              height: Layout.window.height / 3,
              flexDirection: 'row',
            }}>
            <View
              style={{
                backgroundColor: 'rgba(0,0,0,0.5)',
                width: (Layout.window.width - Layout.window.height / 3) / 2,
              }}
            />
            <View
              style={{
                width: Layout.window.height / 3,
              }}
            />
            <View
              style={{
                backgroundColor: 'rgba(0,0,0,0.5)',
                width: (Layout.window.width - Layout.window.height / 3) / 2,
              }}
            />
          </View>
          <View
            style={{
              width: '100%',
              backgroundColor: 'rgba(0,0,0,0.5)',
              height: Layout.window.height / 3,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.pop();
              }}
              style={{
                backgroundColor: colors.PRIMARY,
                justifyContent: 'center',
                alignItems: 'center',
                width: Layout.window.width / 3,
                height: 40,
                borderRadius: 5,
              }}>
              <Text style={{ color: '#fff' }}>Đóng</Text>
            </TouchableOpacity>
          </View>
        </RNCamera>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
