import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../constants/theme';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  screen: {
    width,
    height,
    padding: 20,
  },
  textInput: {
    borderRadius: 8,
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    height: 10,
    marginBottom: 10,
    
  },
  touch: {
    width: '100%',
    height: 50,
    borderRadius: 8,
    backgroundColor: colors.PRIMARY,
    marginTop: 10,
  },
  textTouch: {
    color: colors.WHITE,
    fontSize: 16,
  },
  imageLogo:{
    width:150,
    height:150,
    left:width/4,
    top:-100
  },
});

export default styles;
