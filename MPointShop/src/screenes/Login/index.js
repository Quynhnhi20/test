// @ts-nocheck
import React, { useState, useEffect, Component } from 'react';
import { Text, View, ImageBackground, Dimensions, TouchableOpacity, Image } from 'react-native';
import { Fumi } from 'react-native-textinput-effects';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import Wrapper from '../../components/wrapper';
import styles from './styles';
import images from '../../constants/images';
import { colors } from '../../constants/theme';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { userActions } from "../../state/user";
import storeT from "../../utils/store";
import SplashScreen from 'react-native-splash-screen'
import helpers from '../../globals/helpers';

const { width, height } = Dimensions.get('window');

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      user: '',
      pass: ''
    }
  }

  componentDidMount() {
    this.reFreshToken()
    SplashScreen.hide()
  }


  onLogin() {
    storeT.setPassword(this.state.pass)
    const { login } = this.props
    login({ username: this.state.user, password: this.state.pass, success: this.props.navigation })
  }

  async reFreshToken() {
    let token = await storeT.getToken()
    if (!token) return
    this.props.refreshtoken({ success: this.props.navigation, token })
  }


  render() {
    const { navigation } = this.props
    return (
      <ImageBackground source={images.BGLOGIN} style={{ flex: 1 }}>
        <Wrapper flex={1} middle style={styles.screen}>
          <Image
            source={images.LOGOLOGIN}
            style={styles.imageLogo}
            resizeMode="contain"
          />
          <Fumi
            label={'Tên đăng nhập'}
            iconClass={FontAwesomeIcon}
            iconName={'user'}
            iconColor={colors.WHITE}
            iconSize={20}
            iconWidth={40}
            inputPadding={16}
            labelStyle={{ color: colors.WHITE }}
            inputStyle={{ color: colors.WHITE }}
            style={styles.textInput}
            onChangeText={(userText) => { this.setState({ user: userText }) }}
          />
          <Fumi
            label={'Mật khẩu'}
            iconClass={FontAwesomeIcon}
            iconName={'key'}
            iconColor={colors.WHITE}
            iconSize={20}
            iconWidth={40}
            inputPadding={16}
            labelStyle={{ color: colors.WHITE }}
            inputStyle={{ color: colors.WHITE }}
            style={styles.textInput}
            onChangeText={(passText) => { this.setState({ pass: passText }) }}
          />
          <TouchableOpacity onPress={() => this.onLogin()}>
            <Wrapper center middle style={styles.touch}>
              <Text style={styles.textTouch}>Đăng nhập</Text>
            </Wrapper>
          </TouchableOpacity>
        </Wrapper>
      </ImageBackground>
    )
  }
}

Login.navigationOptions = ({ navigation }) => ({
  header: null,
});

const mapStateToProps = state => ({
  profile: state.user.profile,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      login: userActions.loginUser,
      refreshtoken: userActions.refreshToken
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
