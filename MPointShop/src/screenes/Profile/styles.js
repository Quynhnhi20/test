import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../constants/theme';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  imageLogo: {
    width: 150,
    height: 150,
    // left: width / 3.5,
    // top: 40,
  },
  infomation: {
    marginTop: 10,
  },
  name: {
    fontSize: 20,
    color: colors.WHITE,
    fontWeight: '700',
    marginBottom: 10,
  },
  share: {
    fontSize: 16,
    color: colors.WHITE,
    marginBottom: 10,
  },
  poind: {
    fontSize: 20,
    color: colors.WHITE,
    fontWeight: '700',
  },
  content:{
    padding: 10,
  },
  input:{
    width:'100%',
    height:40,
    borderColor: colors.BLACK,
    borderWidth:0.5,
    borderRadius:5,
    paddingLeft:5,
    marginBottom:8
  },
  touchSub:{
    marginTop:20,
    width:200,
    height:35,
    backgroundColor:colors.PRIMARY_DARK,
    borderRadius:5,
    marginBottom:10
  }
});

export default styles;
