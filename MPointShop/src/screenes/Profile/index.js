import React, {Component} from 'react';
import {
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  TextInput
} from 'react-native';
import Wrapper from '../../components/wrapper';
import images from '../../constants/images';
import styles from './styles';
const {width, height} = Dimensions.get('window');
import {Content, List, ListItem, Right, Left, Icon} from 'native-base';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { userActions } from "../../state/user";
import SelectPopup from '../../components/popups/SelectPopup';
import { colors } from '../../constants/theme';
import storeT from "../../utils/store";



class Profile extends Component {

  constructor(props) {
    super(props);
    this.state={
      popup:false,
      mkCu:'',
      oldPassword:'',
      password:'',
      moiPassword:'',
      errMKMoi:false,
      errMKCu:false,
    }
  }

  
  onSubmit(){
    if (this.state.mkCu !== this.state.oldPassword){
      this.setState({ errMKCu:true})
    } else (this.setState({ errMKCu: false }))
    if (this.state.password !== this.state.moiPassword) {
      this.setState({ errMKMoi: true })
    } else (this.setState({ errMKMoi: false }))
  }

  componentDidMount() {
    const getPassCu = storeT.getPassword().then(data => { this.setState({ mkCu: data })})    
  }

  render() {
    const { navigation, profile } = this.props
    return (
      <Wrapper flex={1}>
        <ImageBackground
          source={images.IMGHEADER}
          style={{
            width,
            height: 0.37 * height,
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={images.USERIMG}
            style={styles.imageLogo}
            resizeMode="contain"
          />
          <Wrapper center middle style={styles.infomation}>
            {/* <Text style={styles.name}>{profile.user.name} || ID: {profile.user.id}</Text> */}
            {/* <Text style={styles.share}>{profile.user.email ? profile.user.email : 'Chưa liên kết email'}</Text> */}
            <Text style={styles.poind}>O điểm</Text>
          </Wrapper>
        </ImageBackground>
        <Wrapper style={styles.content}>
          <List>
            <ListItem selected onPress={() => this.setState({ popup:true})}>
              <Left>
                <Wrapper row center>
                  <Icon name="key"></Icon>
                  <Text style={{ marginLeft: 5 }}>Đổi mật khẩu</Text>
                </Wrapper>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <SelectPopup
              visible={this.state.popup}
              title={'Đổi mật khẩu'}
              close={{ onPress: () => this.setState({ 
                popup: false,
                errMKMoi: false,
                errMKCu: false,
              }) }}
              card={
                <>
                  <Wrapper center middle>
                    <TextInput
                      style={styles.input}
                      placeholder="Nhập mật khẩu cũ (*)"
                      onChangeText={(mkcu) => { this.setState({ oldPassword: mkcu})}}
                    />
                    {
                      this.state.errMKCu 
                      ? <Text style={{ color: 'red', fontSize: 14 }}>Mật khẩu cũ không chính xác</Text>
                      : null
                    }
                    
                    <TextInput
                      style={styles.input}
                      placeholder="Nhập mật khẩu mới (*)"
                      onChangeText={(pass) => { this.setState({ password: pass }) }}
                    />
                    <TextInput
                      style={styles.input}
                      placeholder="Xác nhận lại mật khẩu (*)"
                      onChangeText={(mkMoi) => { this.setState({ moiPassword: mkMoi })}}
                    />
                    {
                      this.state.errMKMoi ?
                        <Text style={{ color: 'red', fontSize: 14 }}>Mật khẩu không trùng khớp</Text>
                      :null
                    }
                    
                    <TouchableOpacity onPress={()=>this.onSubmit()}>
                      <Wrapper center middle style={styles.touchSub}>
                        <Text style={{color:'white'}}>Thay đổi</Text>
                      </Wrapper>
                    </TouchableOpacity>
                  </Wrapper>
                </>
              }
            />
            <ListItem onPress={() => navigation.navigate('EditUserLink')}>
              <Left>
                <Wrapper row center>
                  <Icon name="share-alt"></Icon>
                  <Text style={{ marginLeft: 5 }}>Đổi tài khoản niên kết</Text>
                </Wrapper>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem onPress={() => navigation.navigate('History')}>
              <Left>
                <Wrapper row center>
                  <Icon name="create"></Icon>
                  <Text style={{ marginLeft: 5 }}>Lịch sử đối soát</Text>
                </Wrapper>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem>
              <Left>
                <Wrapper row center>
                  <Icon name="log-out"></Icon>
                  <Text style={{ marginLeft: 5 }}>Đăng xuất</Text>
                </Wrapper>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Version IOS 1.1.2</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
          </List>
        </Wrapper>
      </Wrapper>
    )
  }
}


Profile.navigationOptions = ({navigation}) => ({
  header: null,
});

const mapStateToProps = state => ({
  profile: state.user.profile
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      // history: userActions.historyShop
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);