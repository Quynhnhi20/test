import React, { Component } from 'react';
import {
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView
} from 'react-native';
import { bindActionCreators } from "redux";

import Wrapper from '../../components/wrapper';
import { codeActions } from "../../state/code";
import images from '../../constants/images';
import styles from './styles';
import { connect } from 'react-redux'
import { RNCamera } from 'react-native-camera';
import Ripple from 'react-native-material-ripple';
import { colors } from '../../constants/theme';
import { Icon } from 'native-base'

const { width, height } = Dimensions.get('window');
class Identification extends Component {

  constructor(props) {
    super(props);
    this.state = {
      qr: ''
    }
  }


  async checkOut(code) {
    this.props.getCodeInfo({ code })
  }

  render() {
    const { navigation } = this.props
    return (
      <ScrollView style={{ backgroundColor: '#dddddd' }}>
        <Wrapper flex={1} style={{ backgroundColor: colors.GRAY2 }}>
          <Wrapper style={styles.viewTop}>
            <Image
              style={{ flex: 1 }}
              source={require('../../assets/images/mPointShopv2_src_img_bgheader.jpg')}
            />
          </Wrapper>
          <Wrapper center middle>
            <TouchableOpacity onPress={() => navigation.navigate('QRScan', {
              checkOut: this.checkOut.bind(this)
            })}>
              <Wrapper style={styles.touchScan} center middle>
                <Icon name='qr-scanner' style={{ color: colors.PRIMARY_DARK }} />
                <Text style={{ color: colors.PRIMARY_DARK }}>Quét mã</Text>
              </Wrapper>
            </TouchableOpacity>
            <Text style={styles.or}>Hoặc</Text>
            <Wrapper style={styles.viewInput} center middle>
              <Text style={styles.doiSoat}>Đối soát bằng mã</Text>
              <TextInput style={styles.textInput} onChangeText={(text) => this.setState({ qr: text })}></TextInput>
              <TouchableOpacity>
                <Wrapper center middle style={styles.touchSubmit}>
                  <Text style={styles.xacThuc}>Xác thực</Text>
                </Wrapper>
              </TouchableOpacity>
            </Wrapper>
          </Wrapper>
        </Wrapper>
      </ScrollView>
    )
  }
}


const mapStateToProps = state => ({
  codeInfo: state.codeState.codeInfo,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getCodeInfo: codeActions.getcodeInfo
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Identification);
