import { StyleSheet, Dimensions } from 'react-native';
import { colors } from '../../constants/theme';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  viewTop: {
    width,
    height: 220,
    backgroundColor: colors.PRIMARY_DARK,
    borderBottomLeftRadius: 100,
    borderBottomRightRadius: 100,
    overflow: 'hidden'
  },
  touchScan: {
    width: 130,
    height: 130,
    borderRadius: 100,
    backgroundColor: 'white',
    marginTop: -70
  },
  or: {
    paddingVertical: 20,
    fontSize: 18
  },
  viewInput: {
    width: 0.9 * width,
    height: 200,
    backgroundColor: 'white',
    borderRadius: 20
  },
  touchSubmit: {
    paddingHorizontal: 100,
    paddingVertical: 10,
    backgroundColor: colors.PRIMARY_DARK,
    borderRadius: 10,
  },
  xacThuc: {
    color: 'white'
  },
  doiSoat: {
    color: colors.PRIMARY_DARK,
    fontSize: 16
  },
  textInput: {
    width: '90%',
    height: 40,
    backgroundColor: colors.GRAY2,
    marginVertical: 20,
    paddingLeft: 5
  }
});

export default styles;
