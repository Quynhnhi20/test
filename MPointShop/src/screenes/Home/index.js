import React, { useState, useEffect, Component } from "react";
import {
  StyleSheet,
  View,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Image,
  ImageBackground,
  Dimensions,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Text,
} from 'native-base';
import Wrapper from "../../components/wrapper";
import { colors } from "../../constants/theme";
import styles from "./styles";
import images from '../../constants/images';
// import ImagePicker from 'react-native-image-picker';
import SelectPopup from "../../components/popups/SelectPopup";

const { width, height } = Dimensions.get("window");


class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      imageLove: '',
      popup: false,
    };
  }

  // liblaryImage() {
  //   const options = {
  //     title: 'Photo Picker',
  //     takePhotoButtonTitle: 'Take Photo',
  //     maxWidth: width,
  //     maxHeight: height,
  //     cameraType: 'front',
  //     storageOptions: {
  //       skipBackup: true,
  //       path: 'images',
  //     },
  //   };
  //   ImagePicker.launchImageLibrary(options, response => {
  //     console.log('Response = ', response.uri);
  //     this.setState({
  //       imageLove: response.uri,
  //     });

  //     // if (response.didCancel) {
  //     //   console.log('User cancelled image picker');
  //     // } else if (response.error) {
  //     //   console.log('ImagePicker Error: ', response.error);
  //     // } else if (response.customButton) {
  //     //   console.log('User tapped custom button: ', response.customButton);
  //     // } else {
  //     //   const source = {uri: response.uri};

  //     //   // You can also display the image using data:
  //     //   // const source = { uri: 'data:image/jpeg;base64,' + response.data };

  //     //   // this.setState({
  //     //   //   avatarSource: source,
  //     //   // });
  //     // }
  //   });
  // }

  render() {

    const {navigation} = this.props

    return (
      <Container style={{flex: 1}}>
        <Header style={styles.header}>
          <Left style={styles.left}>
            <Button transparent onPress={()=>{navigation.openDrawer()}}>
              <Icon name="menu" style={styles.iconHome} />
            </Button>
          </Left>
          <Body style={styles.body}>
            <Title style={styles.title}>Love Days Counter</Title>
          </Body>
          <Right style={styles.right}>
            <Button transparent>
              <Icon name="flag" style={styles.iconHome} />
            </Button>
            <Button transparent>
              <Icon name="flag" style={styles.iconHome} />
            </Button>
          </Right>
        </Header>
        <Content>
          <View style={styles.viewDemNgay}>
            <Image
              source={images.IMGHUG3}
              style={styles.imgCAY}
              resizeMode="contain"
            />
            <Text style={styles.day}>100 Day</Text>
          </View>
          <View style={styles.viewContact}>
            <View style={styles.viewContactLeft}>
              <ImageBackground
                source={images.IMGCRCLE}
                style={{
                  height: 140,
                  width: 140,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                resizeMode="cover">
                <View style={styles.borderContact}>
                  <Image
                    source={images.IMGBOY}
                    style={styles.iconItem}
                    resizeMode="cover"
                  />
                </View>
              </ImageBackground>
            </View>
            <View style={styles.viewBody}>
              <Image
                source={images.IMGLOVE}
                style={styles.imgLa}
                resizeMode="contain"
              />
            </View>
            <View style={styles.viewContactRight}>
              <ImageBackground
                source={images.IMGCRCLE}
                style={{
                  height: 140,
                  width: 140,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                resizeMode="cover">
                <View style={styles.borderContact}>
                  <Image
                    source={images.IMGGIRL}
                    style={styles.iconItem}
                    resizeMode="cover"
                  />
                </View>
              </ImageBackground>
            </View>
          </View>
          <View style={styles.viewText}>
            <View style={[styles.textItem, {flex: 3}]}>
              <Text style={[styles.ten, {fontWeight: '500'}]}>
                Nguyễn Văn A
              </Text>
              <View style={styles.viewChiTiet}>
                <Icon name="male" style={styles.iconMale}></Icon>
                <Text style={styles.ten}>18 old</Text>
                <Text style={[styles.ten, {fontSize: 10}]}> (Nhân mã)</Text>
              </View>
            </View>
            <View style={styles.textItem} />
            <View style={[styles.textItem, {flex: 3}]}>
              <Text style={[styles.ten, {fontWeight: '500'}]}>Nguyễn Thị B</Text>
              <View style={styles.viewChiTiet}>
                <Icon name="female" style={styles.iconMale}></Icon>
                <Text style={styles.ten}>18 old</Text>
                <Text style={[styles.ten, {fontSize: 10}]}> (Bảo bình)</Text>
              </View>
            </View>
          </View>
          <View style={styles.borderImgTrangTri}>
            <Image
              source={images.IMGLA}
              style={styles.imgLa}
              resizeMode="contain"
            />
          </View>
          <View style={styles.viewFlowerimg}>
            {/* <TouchableOpacity onPress={() => this.liblaryImage()}>
              <Text>Chonj anh</Text>
            </TouchableOpacity> */}
            {this.state.imageLove ? (
              <Image
                style={{width: '100%', height: 200, borderRadius: 8}}
                source={{uri: this.state.imageLove}}
                resizeMode="contain"
              />
            ) : null}
          </View>
          {/* <View style={styles.borderImgTrangTri}>
            <Image
              source={images.HAT}
              style={styles.imgLa}
              resizeMode="contain"
            />
          </View> */}
          <View style={styles.viewFlowerimg}>
            <Image
              source={images.IMGFLOWERTOP}
              style={styles.flowerimg}
              resizeMode="cover"
            />
          </View>
          <View style={styles.viewFlowerimg}>
            <TouchableOpacity
              onPress={() => {
                this.setState({popup: true});
              }}>
              <Text>popup</Text>
            </TouchableOpacity>
          </View>
          <SelectPopup
            visible={this.state.popup}
            title="Thông tin hiển thị"
            close={{onPress: () => this.setState({popup: false})}}
            card={
              <Text>hahah</Text>
            }
            // buttons={[
            //   {
            //     title: 'OK',
            //     // color: colors.COLOR_PRIMARY,
            //     onPress: () => this.setState({popup: !this.state.popup}),
            //   },
            // ]}
          />
          <View style={styles.viewSlogan}>
            <Text style={styles.textSlogan}>
              You are my love and it is true, I’ll do all for you that I can do.
            </Text>
          </View>
          <View style={styles.viewFlowerimg}>
            <Image
              source={images.IMGFLOWERBOTTOM}
              style={[styles.flowerimg, {marginTop: 10}]}
              resizeMode="cover"
            />
          </View>
          <View style={styles.borderImgTrangTri}>
            <Image
              source={images.IMGLA1}
              style={styles.imgLa}
              resizeMode="contain"
            />
          </View>
          <View style={styles.viewFlowerimg}>
            <Image
              source={images.IMGFLOWERTOP1}
              style={styles.flowerimg1}
              resizeMode="cover"
            />
          </View>
          <View style={styles.viewTimeDay}>
            <Text style={styles.textSlogan}>Đồng hồ tình yêu</Text>
            <View style={styles.viewDay}>
              <ImageBackground
                source={images.HEART}
                style={styles.heartIcon}
                resizeMode="contain">
                <Text style={styles.time}>00</Text>
              </ImageBackground>
              <ImageBackground
                source={images.HEART}
                style={styles.heartIcon}
                resizeMode="contain">
                <Text style={styles.time}>11</Text>
              </ImageBackground>
              <ImageBackground
                source={images.HEART}
                style={styles.heartIcon}
                resizeMode="contain">
                <Text style={styles.time}>00</Text>
              </ImageBackground>
              <ImageBackground
                source={images.HEART}
                style={styles.heartIcon}
                resizeMode="contain">
                <Text style={styles.time}>04</Text>
              </ImageBackground>
              <ImageBackground
                source={images.HEART}
                style={styles.heartIcon}
                resizeMode="contain">
                <Text style={styles.time}>22</Text>
              </ImageBackground>
              <ImageBackground
                source={images.HEART}
                style={styles.heartIcon}
                resizeMode="contain">
                <Text style={styles.time}>45</Text>
              </ImageBackground>
              <ImageBackground
                source={images.HEART}
                style={styles.heartIcon}
                resizeMode="contain">
                <Text style={styles.time}>55</Text>
              </ImageBackground>
            </View>
            <View style={styles.ViewTextTime}>
              <View style={styles.viewDate}>
                <Text style={styles.textTime}>Năm</Text>
              </View>
              <View style={styles.viewDate}>
                <Text style={styles.textTime}>Tháng</Text>
              </View>
              <View style={styles.viewDate}>
                <Text style={styles.textTime}>Tuần</Text>
              </View>
              <View style={styles.viewDate}>
                <Text style={styles.textTime}>Ngày</Text>
              </View>
              <View style={styles.viewDate}>
                <Text style={styles.textTime}>Giờ</Text>
              </View>
              <View style={styles.viewDate}>
                <Text style={styles.textTime}>Phút</Text>
              </View>
              <View style={styles.viewDate}>
                <Text style={styles.textTime}>Giây</Text>
              </View>
            </View>
            <Text style={[styles.textSlogan, {marginTop: 10}]}></Text>
          </View>
          <View style={styles.viewFlowerimg}>
            <Image
              source={images.IMGFLOWERBOTTOM1}
              style={[styles.flowerimg1, {marginTop: 10}]}
              resizeMode="cover"
            />
          </View>
        </Content>
      </Container>
    );
  }
}
export default Home


// Home.navigationOptions = ({ navigation }) => ({
//   header: null
// });