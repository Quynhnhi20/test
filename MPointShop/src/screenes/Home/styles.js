import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../constants/theme";
const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'white',
    borderBottomWidth: 0,
  },
  iconHome: {
    color: 'black',
  },
  left: {
    flex: 1,
  },
  body: {
    flex: 5,
  },
  right: {
    flex: 1,
  },
  title: {
    fontSize: 18,
    fontWeight: '400',
  },
  content: {

  },
  view: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: '100%',
    position: 'absolute',
    top: 270,
    left: 19
  },
  imgAnimation: {
    width: "100%",
    height: 200,
  },
  iconItem: {
    height: 100,
    width: 100,
    borderRadius: 50
  },
  backgroundImage: {
    flex: 1,
    height: 650
  },
  footer: {
    height: 39
  },
  viewContact: {
    flexDirection: 'row',
    marginTop: 50,
  },
  viewContactLeft: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewBody: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewContactRight: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  borderContact: {
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 60,
  },
  imgDongDien: {
    width: '100%',
  },
  viewText: {
    flexDirection: 'row',
    marginTop: 0,
  },
  textItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',

  },
  ten: {
    fontSize: 14,
    fontWeight: '300',
    textAlign: 'center',
  },
  separator: {
    height: 40,
  },
  textSex: {
    color: 'white'
  },
  contentListModal: {
    paddingTop: 50,
    width: '100%',
    height: '100%',
    backgroundColor: '#c8cdce',
  },
  viewClose: {
    alignItems: 'flex-end'
  },
  iconClose: {
    marginRight: 10
  },
  textListModal: {
    fontSize: 14,
    fontWeight: '300'
  },
  listItem: {
    borderBottomColor: 'white',
  },
  viewDemNgay: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgCAY: {
    width: 150,
    height: 150
  },
  imgHeart: {
    width: 400,
    height: 400,
    marginTop: -40,
  },
  day: {
    fontSize: 30,
    fontWeight: '200',
  },
  viewSlogan: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    backgroundColor: 'white',
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 10,
    paddingBottom: 10,
    paddingTop: 10,
    // borderWidth: 1,
    // borderColor: 'black'
  },
  textSlogan: {
    fontSize: 15,
    fontWeight: '300',
    textAlign: 'center',
    color: 'black'
  },
  viewChiTiet: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
  },
  iconMale: {
    fontSize: 20,
    marginRight: 5,
  },
  viewTimeDay: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    backgroundColor: 'white',
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 10,
    // paddingBottom: 10,
    // paddingTop: 10,
    // borderWidth: 1,
    // borderColor: 'black'
  },
  viewDay: {
    marginTop: 10,
    flexDirection: 'row',
  },
  time: {
    fontSize: 10,
    color: 'black'
  },
  heartIcon: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 50,
    height: 50
  },
  ViewTextTime: {
    flexDirection: 'row',
  },
  viewDate: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  textTime: {
    fontSize: 12,
    color: 'black',
    fontWeight: '300',
  },
  borderImgTrangTri: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    marginTop: 10,
  },
  imgLa: {
    width: 100,
  },
  viewModalThongTin: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  thongTin: {
    width: '80%',
    backgroundColor: '#c8cdce',
    borderRadius: 10
  },
  headerModal: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    flexDirection: 'row',
    backgroundColor: 'gray',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  textInput: {
    width: '100%',
    height: 30,
    borderColor: 'white',
    borderWidth: 1,
    paddingLeft: 5,
    backgroundColor: 'white',
    borderRadius: 5
  },
  touchBirthday: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '70%',
    padding: 5,
    flexDirection: 'row',
    borderRadius: 5,
    borderColor: 'white',
    borderWidth: 1,
    backgroundColor: 'gray'
  },
  buttonSex: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 90,
    borderColor: 'white',
    backgroundColor: 'gray',
  },
  boottomCF: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 5,
    paddingLeft: 10,
    paddingBottom: 5,
    paddingRight: 10,
    paddingTop: 5,
    marginTop: 10,
    backgroundColor: 'gray'
  },
  textCF: {
    fontSize: 14,
    color: 'white'
  },
  viewFlowerimg: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  flowerimg: {
    width: '100%',
    height: 200
  },
  flowerimg1: {
    width: '100%',
  }
});

export default styles;
