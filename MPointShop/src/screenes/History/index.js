import React, {Component} from 'react';
import {Text, View} from 'react-native';
import Wrapper from '../../components/wrapper';
import styles from './styles';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Left,
  Body,
  Right,
  Button,
  Icon,
} from 'native-base';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { userActions } from "../../state/user";

class History extends Component {

  constructor(props) {
    super(props);
    this.state={
      skip:0,
      limit:10
    }
  }
  
  componentDidMount(){
    const { history } = this.props
    history({ skip:this.state.skip, limit:this.state.limit})
  }

  render() {
    return (
      <Container>
        <Header />
        <Content>
          <List>
            <ListItem thumbnail >
              <Left>
                <Thumbnail
                  style={{ width: 100, height: 56, borderRadius: 0 }}
                  source={{
                    uri:
                      'https://media.ngoisao.vn/resize_580/news/2018/11/26/hotgirl-1-ngoisao.vn-w600-h493.jpg',
                  }}
                />
              </Left>
              <Body>
                <Text>Sankhadeep</Text>
                <Text note numberOfLines={1}>
                  Its time to build a difference . .
                </Text>
              </Body>
              <Right>
                <Button transparent>
                  <Icon name='arrow-forward'></Icon>
                </Button>
              </Right>
            </ListItem>
          </List>
        </Content>
      </Container>
    )
  }
}


const mapStateToProps = state => ({
  // profile: state.user.profile,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      history: userActions.historyShop
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(History);