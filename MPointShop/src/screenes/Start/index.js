import { Container, Content, Icon } from "native-base";
import React, { useState, useEffect } from "react";
import {
  Dimensions,
  StyleSheet,
} from "react-native";
import Wrapper from "../../components/wrapper";
import { colors } from "../../constants/theme";

const { width, height } = Dimensions.get("window");


const Start = (
  { navigation } = this.props
) => {

  useEffect(() => {
    setTimeout(() => { navigation.navigate('Login') }, 3000)
  });

  return (
    <Container style={{backgroundColor:'red'}}>
      <Content
        contentContainerStyle={{ flex: 1 }}
      >
      </Content>
    </Container>
  );
}

// EndSignUp.navigationOptions = ({ navigation }) => ({
//   header: null
// });

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.WHITE,
    padding: 20,
    paddingVertical: 30
  },
});

export default Start