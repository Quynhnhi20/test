import { StyleSheet, Dimensions } from 'react-native';
import { colors } from '../../constants/theme';
const { width, height } = Dimensions.get('window');
import { robotoWeights } from 'react-native-typography'

const styles = StyleSheet.create({
  imageLogo: {
    width: 150,
    height: 150,
    marginTop: 30,
  },
  item:{
    width:'100%',
    height:0.15*height,
    // backgroundColor:'red',
    borderRadius:20,
    marginBottom: 10,
  },
  xacThuc:{
    fontSize:16,
    // ...robotoWeights.bold,
    color: 'white',
  },
  iconItem: {
    width: 60,
    height: 60,
  },
  titleItem: {
    fontSize: 20,
    marginBottom: 5,
    color:'white'
  },
  contentTextItem: {
    fontSize: 14,
    color: 'white'
  },
});

export default styles;
