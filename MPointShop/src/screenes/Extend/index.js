import React, { Component } from 'react';
import {
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import Wrapper from '../../components/wrapper';
import images from '../../constants/images';
import styles from './styles';
const { width, height } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Ionicons';
import { RNCamera } from 'react-native-camera';
import Ripple from 'react-native-material-ripple';
import { colors } from '../../constants/theme';

class Extend extends Component {
  render() {
    const { navigation } = this.props
    return (
      <Wrapper flex={1} style={{ padding: 10 }}>
        <Image
          source={images.LOGOLOGIN}
          style={styles.imageLogo}
          resizeMode="contain"
        />
        <Ripple onPress={() => navigation.navigate('Endow')}>
          <Wrapper style={[styles.item, { backgroundColor: '#a67107' }]} row>
            <Wrapper center middle flex={1}>
              <Image
                source={images.ICONQRCODE}
                style={[styles.iconItem, { width: 50, height: 50 }]}
                resizeMode="contain"
              />
            </Wrapper>
            <Wrapper flex={3} middle>
              <Text style={styles.titleItem}>Xác thực mã ưu đãi</Text>
              <Text style={styles.contentTextItem}>
                Xác thực mã ưu đãi được lấy từ app mPoint
              </Text>
            </Wrapper>
          </Wrapper>
        </Ripple>
        <Ripple onPress={() => navigation.navigate('Invoice')}>
          <Wrapper style={[styles.item, { backgroundColor: '#a25d57' }]} row>
            <Wrapper center middle flex={1}>
              <Image
                source={images.ICONSMARTPHONE}
                style={[styles.iconItem, { width: 50, height: 50 }]}
                resizeMode="contain"
              />
            </Wrapper>
            <Wrapper flex={3} middle>
              <Text style={styles.titleItem}>Xác thực mã hóa đơn</Text>
              <Text style={styles.contentTextItem}>
                Xác thực mã hóa đơn của bạn
              </Text>
            </Wrapper>
          </Wrapper>
        </Ripple>
        <Ripple onPress={() => navigation.navigate('Phone')}>
          <Wrapper style={[styles.item, { backgroundColor: '#618F9E' }]} row>
            <Wrapper center middle flex={1}>
              <Image
                source={images.ICONPHONENUMBER}
                style={[styles.iconItem, { width: 50, height: 50 }]}
                resizeMode="contain"
              />
            </Wrapper>
            <Wrapper flex={3} middle>
              <Text style={styles.titleItem}>Xác thực bằng số điện thoại</Text>
              <Text style={styles.contentTextItem}>
                Xác thực bằng số điện thoại của khách hàng
              </Text>
            </Wrapper>
          </Wrapper>
        </Ripple>
      </Wrapper>
    )
  }
}


export default Extend;

// Identification.navigationOptions = ({ navigation }) => ({
//   header: null,
// });
