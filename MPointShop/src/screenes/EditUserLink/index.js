import React, { Component, useState } from 'react';
import {
  Text,
  View,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import images from '../../constants/images';
import Wrapper from '../../components/wrapper';
import styles from './styles';
import { colors } from '../../constants/theme';
import { Icon } from 'native-base';
const { width, height } = Dimensions.get('window');

const EditUserLink = () => {
  const [CodeInvoice, setCodeInvoice] = React.useState('');
  const [Invoice, setInvoice] = React.useState('');
  const [Phone, setPhone] = React.useState('');

  return (
    <ImageBackground source={images.BGLOGIN} style={{ width, height }}>
      <Wrapper flex={1} style={{ padding: 10 }}>
        <Wrapper style={styles.content}>
          <Wrapper style={{ paddingVertical:10}}>
            <Text>Để thực hiện gắn mã thẻ tích điểm. Bạn cần xác thực số điện thoại của 2 bước:</Text>
            <Text>1. Nhập số điện thoại để xác thực</Text>
            <Text>2. Nhập mã xác thực otp nhận được trên điện thoại</Text>
          </Wrapper>
          <Wrapper row center style={styles.item}>
            <Icon name="call" style={styles.icon}></Icon>
            <TextInput
              style={styles.textInvoice}
              onChangeText={textInvoice => setInvoice(textInvoice)}
              value={Invoice}
              placeholder="Nhập số điện thoại"
              placeholderTextColor={colors.GRAY}
            />
          </Wrapper>
        </Wrapper>
        <TouchableOpacity>
          <Wrapper style={styles.touchSubmit} center middle>
            <Text style={styles.textSubmit}>Gửi mã xác nhận</Text>
          </Wrapper>
        </TouchableOpacity>
      </Wrapper>
    </ImageBackground>
  );
};

export default EditUserLink;

EditUserLink.navigationOptions = ({ navigation }) => ({
  title: 'Thay đổi tài khoản liên kết',
  headerStyle: {
    backgroundColor: colors.PRIMARY_DARK,
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
});
