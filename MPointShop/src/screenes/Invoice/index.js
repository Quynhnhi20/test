import React, {Component, useState} from 'react';
import {
  Text,
  View,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import images from '../../constants/images';
import Wrapper from '../../components/wrapper';
import styles from './styles';
import {colors} from '../../constants/theme';
import {Icon} from 'native-base';
const {width, height} = Dimensions.get('window');

const Invoice = () => {
  const [Invoice, setInvoice] = React.useState('');

  return (
    <ImageBackground source={images.BGLOGIN} style={{width, height}}>
      <Wrapper flex={1} style={{padding: 10}}>
        <Wrapper style={styles.content}>
          <Wrapper row center style={styles.item}>
            <Icon name="contact" style={styles.icon}></Icon>
            <TextInput
              style={styles.textEndow}
              onChangeText={textInvoice => setInvoice(textInvoice)}
              value={Invoice}
              placeholder="Nhập mã hóa đơn"
              placeholderTextColor={colors.GRAY}
            />
            <Wrapper flex={1} center middle>
              <Text style={styles.or}>Hoặc</Text>
            </Wrapper>
            <TouchableOpacity>
              <Wrapper center middle style={styles.touchScan} flex={1}>
                <Text style={styles.textScan}>Quét QR</Text>
              </Wrapper>
            </TouchableOpacity>
          </Wrapper>
        </Wrapper>
        <TouchableOpacity>
          <Wrapper style={styles.touchSubmit} center middle>
            <Text style={styles.textSubmit}>Kiểm tra</Text>
          </Wrapper>
        </TouchableOpacity>
      </Wrapper>
    </ImageBackground>
  );
};

export default Invoice;

Invoice.navigationOptions = ({navigation}) => ({
  title: 'Xác thực mã hóa đơn',
  headerStyle: {
    backgroundColor: colors.PRIMARY_DARK,
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
});
