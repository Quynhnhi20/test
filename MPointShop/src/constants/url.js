const baseUrl = "http://prod.mpoint.vn/";
const prefix = "app/thanhvd";
const endpoint = {
  HISTORYSHOP: "code/shopGetHistories?page=57&api=shopGetHistories",
  LOGIN: "user/login",
  ME: "auth/mobile/employee",
  WORK_PLACES: "mobile/workplaces",
  SIGN_UP: "mobile/register_employee",
  CURRENT_WORK_PLACES: "mobile/current-workplace",
  STATUS_CHECKIN: "mobile/checking-status",
  REGISTER: "cuahang/dangky",
  CHECKIN: "mobile/checkin",
  CHECKOUT: "mobile/checkout",
  SESSION: "histories",
  UPLOAD: "mobile/upload/checkin",
  WORK_CALENDAR: "mobile/work-calendars",
  REFRESH_TOKEN: "user/refreshToken",
  GET_CODE_INFO: "code/checkCodeInfo?page=57&adpi=checkCodeInfo",

};

export { baseUrl, endpoint, prefix };
