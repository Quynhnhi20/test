import axios from "axios";
import { baseUrl, endpoint, prefix } from "../constants/url";
import getUniqueString from "./getUniqueString";
import storeT from "../utils/store";
import moment from 'moment'

axios.defaults.baseURL = baseUrl;

export const setAuthToken = token => {
  console.log(token)
  if (token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  } else {
    delete axios.defaults.headers.common["Authorization"];
  }
};

export async function getCompany(code_company) {
  try {
    const res = await axios.get(`/${endpoint.GET_COMPANY}/${code_company}`);
    return res.data;
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function getWorkPlace() {
  try {
    const res = await axios.get(`/${endpoint.WORK_PLACES}`);
    // console.log(res.data);
    return res.data;
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function getCurrentWorkPlace() {
  try {
    const res = await axios.get(`/${endpoint.CURRENT_WORK_PLACES}`);
    // console.log(res.data);
    return res.data;
  } catch (error) {
    return Promise.reject(error);
  }
}
export async function getStatusCheckin() {
  try {
    const res = await axios.get(`/${endpoint.STATUS_CHECKIN}`);
    console.log(res.data);
    return res.data;
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function getUserInfo() {
  try {
    const res = await axios.get(`/${endpoint.ME}`);
    // console.log(res.data)
    return res.data;
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function login({ username, password }) {
  try {
    const res = await axios.post(`/${endpoint.LOGIN}`, {
      username, password
    });
    return res.data;
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function getCodeInfo({ code }) {
  try {
    const res = await axios.post(`/${endpoint.GET_CODE_INFO}`, {
      code
    });
    return res.data;

  } catch (error) {
    return Promise.reject(error);
  }
}


export async function refreshToken() {
  try {
    const res = await axios.post(`/${endpoint.REFRESH_TOKEN}`, {
    });
    return res.data;
  } catch (error) {
    return Promise.reject(error);
  }
}


export async function history({ skip, limit }) {
  try {
    const res = await axios.post(`/${endpoint.HISTORYSHOP}`, {
      skip, limit
    });
    return res.data;
  } catch (error) {
    return Promise.reject(error);
  }
}

export const uploadImg = async ({ uri, name, size, path }) => {
  const formData = new FormData();

  formData.append("image", {
    uri,
    type: "image/jpg",
    name
  });

  const config = {
    headers: {
      "Content-Type": "multipart/form-data"
    }
  };
  return new Promise((resolve, reject) => {
    axios.post(endpoint.UPLOAD, formData, config).then(
      response => {
        // console.log(`task: `, response.data);
        resolve(response.data);
      },
      error => {
        console.log(error);
        reject(error);
      }
    );
  });
};

export async function checkin({ work_calendar_id, work_place_id, lat_check_in, long_check_in, image_check_in, checking_code }) {
  try {
    // console.log(work_calendar_id, work_place_id, lat_check_in, long_check_in, image_check_in, checking_code);
    const res = await axios.post(`/${endpoint.CHECKIN}`, {
      work_calendar_id,
      work_place_id,
      lat_check_in,
      long_check_in,
      image_check_in,
      checking_code
    });
    // console.log(res.data)
    return res.data;
  } catch (error) {
    console.log(error);
    return false;
  }
}

export async function checkout({ checking_id, ...rest }) {
  try {
    const res = await axios.post(`/${endpoint.CHECKOUT}/${checking_id}`, rest);
    // console.log(res.data)
    return res.data;
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function getSession() {
  try {
    const res = await axios.get(`/${endpoint.SESSION}`);
    return res.data.result;
  } catch (error) {
    return false;
  }
}

export async function callSignUp(payload) {
  const res = await axios.post(`/${endpoint.SIGN_UP}`, payload);
  // console.log(res.data);
  return res.data;
}

export async function getWorkCalendar({ startTime, endTime }) {
  try {
    const start = moment(startTime).format('DD-MM-YYYY');
    const end = moment(endTime).format('DD-MM-YYYY');
    const res = await axios.get(`/${endpoint.WORK_CALENDAR}?start_date=${start}&end_date=${end}`);
    return res.data.data;
  } catch (error) {
    console.log(error.response)
    return Promise.resolve([])
  }
}
