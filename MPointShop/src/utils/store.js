import AsyncStorage from "@react-native-community/async-storage";

class Store {
  __token = "";
  __company_id = "";
  __refresh_token = "";

  async getToken() {
    return this.__token || (await AsyncStorage.getItem("user_mpoint_token"));
  }

  async setToken(token) {
    this.__token = token;
    await AsyncStorage.setItem("user_mpoint_token", token);
  }

  async getPassword() {
    return await AsyncStorage.getItem("set_pass")
  }

  async setPassword(pass) {
    await AsyncStorage.setItem("set_pass", pass);
  }

  
}

export default new Store();
